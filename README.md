**Distributed Implementations of timed Component-based Systems.**


**Brief Info:** <br>
We are currently looking for a motivated Master Student who would like to work at Huawei Grenoble Research Center in collaboration with Verimag, University of Grenoble Alpes (UGA). 

**Contacts:** <br> S. Bensalem (Saddek.Bensalem@univ-grenoble-alpes.fr) and F. Yexiang (yexiang4@huawei.com) 

**Scientific Context:**<br>
Real-time systems are systems that are subject to ”real-time constraints”. The correctness of such systems depends not only on the logical results of the actions, but also on the physical instants at which these results are produced. Correctness of real-time system includes the satisfaction of its timing constraints, that is, the instants at which actions are produced have to meet specific conditions. Timing constraints are usually expressed by user-defined bounds in which the system has to react (i.e. executing actions), e.g. deadlines. There are two main classes of real-time systems: hard real-time systems where it is absolutely imperative that the system react within the specified bounds; and soft real-time where response times are important but the system can still function if the constraints are occasionally violated. Meeting timing constraints depends on features of the execution platform such as its speed. For instance, when the platform is not speed enough, the timing constraints may be violated. Worst Case Execution Time (WCET) analysis can be performed in order to verify whether the target platform is fast enough to respect the timing constraints. The building process of real-time systems includes in general two essential steps, modeling and implementation. In the modeling phase, the real-time system specifications are transformed into a high-level model. These models are written in high-level programming languages, sometimes with formal semantics. Those languages provide high-level primitives used for components coordination. 
In this work, we will consider distributed real-time applications using the BIP Framework. BIP [BBB+11] (Behavior, Interaction, Priority) is a model-based and component-based framework where systems consist of sets of components, subject to timing constraints and coordinating their actions through multiparty interactions [JS96], that is synchronizations between possibly more than two components. The second phase towards building a real-time system is to derive an implementation from the high-level model. Implementation compromises the abstractions of the high-level model. For instance, in high-level models, actions are assumed to take zero time. Nonetheless, in the implementation actions may take arbitrary execution times. This may lead the system to not meet the timing constraints specified in the high-level model. The implementation could be either centralized or distributed. This depends on the topology of the underlying architecture on which the real-time system will be deployed. In this thesis, we are interested in distributed implementation of real-time systems.

Distributed implementation of systems is challenging and complex when systems are subject to timing constraints, that is, for real-time systems. This is due to the fact that one not only has to consider typical problems of distributed systems (e.g. coordination and/or synchronization), but also should ensure that all subtle interleavings of the system meet the timing constraints. In addition, distributed implementation of real-time systems has to cope with specific issues such as clocks drift [Lamport78]. Thus, components may not have the same reference of time, which may discard system consistency, e.g. strict ordering of operations may be compromised. To overcome this problem, usual solutions rely on clock synchronization protocols [MBG+04, LEW+05] that guarantee the synchronization of clocks up to given precision. Distributed implementation of a multiparty interactions results in solving the committee coordination problem [CM88], where a set of professors are organized in a set of committees and two committees can meet concurrently only if they have no professor in common; i.e., they are not conflicting. Conflict resolution is the main obstacle in distributed implementation of multiparty interactions.

**Research Directions:**<br>
The Student will work on the following tasks:
1.	Knowledge Based Reduction of Potentially Conflicting Interaction: In general, conflicts can be very hard to characterize for real life case studies since they depend on the reachability of particular states. The computation of the conflicting interactions relies on over-approximations. In this direction, the goal is to obtain the best over-approximation by developing new reduction techniques of potentially conflicting interaction.

2.	Communication Delays: Distributed real-time systems are prone to different kind of problems. The immediate concern is the communication delays inherent to distributed platforms. The latter increase considerably the effort of coordinating the parallel activities of running components. Thus, scheduling such systems must cope with the induced delays by proposing execution strategies ensuring global consistency while satisfying the imposed timing constraints.

3.	Clock Drift: Clock drift is another phenomenon intrinsic to distributed platforms. A clock is a device that consists of a counter that is incremented periodically according to the frequency of an oscillator. This implies that clocks are not perfect since the oscillator frequencies may vary during their lifetime due to several factors such as aging, temperature, humidity, etc. Consequently, clocks trend to drift or gradually desynchronize from a given reference time. Moreover, when having multiple clocks running in the same system, which is usually the case in distributed real-time systems, the relative clock drift between these clocks may result in an unexpected (even undesirable) behavior.

**Requirements:**<br>
The candidate is required to have a Master degree in computer science or electrical computer engineering and preferably should have a strong background in model-based design and programming, as well as in real-time systems theory and operating systems. Efficient communication skills (oral and written) in English are required.

Work position and application:
The work will primarily take place in Huawei Grenoble Research Center (GRC) under local supervision of Dr Yexian Franck for Huawei and the academic supervision of Prof. Saddek Bensalem at Verimag, MSTII Doctoral School. Huawei GRC provides a high-quality scientific environment, travel funds, and salary competitive with similar positions in French industry. The 3-year CIFRE contract will be setup conditioned on successful review by ANRT and doctoral registration.

The complete application consists of the following documents, which should be sent as PDF files:
- 	Your resume (CV)
- 	One-page cover letter (clearly indicating available start date as well as relevant qualifications, experience and motivation)
- 	University certificates and transcripts BSc and MSc degrees
- 	Contact details of up to two referees

**References :**<br>

- [BBB+11b] Ananda Basu, Saddek Bensalem, Marius Bozga, Jacques Combaz, Mohamad Jaber, Thanh-Hung Nguyen, and Joseph Sifakis. Rigorous component-based system design using the BIP framework. IEEE Software, 28(3):41–48, 2011.

- [JS96] Yuh-Jzer Joung and Scott A Smolka. A comprehensive study of the complexity of multiparty interaction. Journal of the ACM (JACM), 43(1):75–115, 1996.

- [Lamport78] L. Lamport. Time, clocks, and the ordering of events in a distributed system. Communications of the ACM, 21(7):558–565, July 1978.

- [MBG+04] Miklos Maroti, Branislav Kusy, Gyula Simon, and Akos Ledeczi. The flooding time synchronization protocol. In Proceedings of the 2nd international conference on Embedded networked sensor systems, pages 39–49. ACM, 2004.

- [LEW+05] K Lee, John C Eidson, Hans Weibel, and Dirk Mohl. Ieee 1588-standard for a precision clock synchronization protocol for networked measurement and control systems. In Conference on IEEE, volume 1588, page 2, 2005.

- [CM88] K. M. Chandy and J. Misra. Parallel program design: a foundation. Addison-Wesley
 Longman Publishing Co., Inc., Boston, MA, USA, 1988. ISBN 0-201-05866-9.
